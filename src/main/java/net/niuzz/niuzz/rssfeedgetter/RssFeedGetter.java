package net.niuzz.niuzz.rssfeedgetter;

import java.net.URL;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.hibernate.classic.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

import net.niuzz.niuzz.utils.HibernateUtil;
import net.niuzz.niuzz.utils.URLConnectionReader;

public class RssFeedGetter implements GettingRssFeed {
	static Logger logger = Logger.getLogger(RssFeedGetter.class);

	public void getRssFeed() {
		PropertyConfigurator.configure("log4j.properties");
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		String urlString = String.format("http://niuzz.net/api/hccrawler/get_sources/");
		String user = "here you should normally put user";
		String password = "here you should normally put password";
		logger.info("url = " + urlString);

		String content = null;
		try {
			content = URLConnectionReader.getText(urlString, user, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("content = " + content);

		JSONObject obj = new JSONObject(content);
		JSONArray arr = obj.getJSONArray("sources");

		// session.beginTransaction().begin();
		for (int rssFeedIndex = 0; rssFeedIndex < arr.length(); rssFeedIndex++) {
			String rssFeedUrlString = String.format(arr.getString(rssFeedIndex));
			logger.info("rssFeedUrl = " + rssFeedUrlString);

			try {
				SyndFeedInput input = new SyndFeedInput();

				URL rssFeedUrl = new URL(rssFeedUrlString);
				SyndFeed rssFeed = input.build(new XmlReader(rssFeedUrl));

				for (Iterator i = rssFeed.getEntries().iterator(); i.hasNext();) {
					SyndEntry entry = (SyndEntry) i.next();
					String entryTitle = entry.getTitle();
					String entryDescription = entry.getDescription().getValue().replaceAll("<p>", "").replaceAll("</p>", "").split("Artyku� <a rel")[0];
					String entryLink = entry.getLink();
					logger.info(entryTitle + "|" + entryDescription + "|" + entryLink +"\n");
				}

				// logger.info("rssFeed = " + rssFeed);

			} catch (Exception e) {
				e.printStackTrace();
				logger.error("ERROR: " + e.getMessage());
			}
		}
		// session.getTransaction().commit();
	}
}